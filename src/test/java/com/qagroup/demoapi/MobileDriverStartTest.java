package com.qagroup.demoapi;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.github.javafaker.Faker;
import com.qagroup.apidemos.AlertDialogsScreen;
import com.qagroup.apidemos.ApiDemosApp;
import com.qagroup.apidemos.AppOptionsScreen;
import com.qagroup.apidemos.MainScreen;
import com.qagroup.tools.WebApp;
import com.qagroup.tools.WebAppTest;

public class MobileDriverStartTest implements WebAppTest {

	ApiDemosApp apiDemosApp = new ApiDemosApp();
	private MainScreen mainScreen;
	private AppOptionsScreen appOptionsScreen;
	private AlertDialogsScreen alertDialogsScreen;

	private String username;

	@BeforeClass
	public void dataSetup() {
		username = new Faker().name().username();
	}

	@Test
	public void apidemosNavigationTest() {
		mainScreen = apiDemosApp.openMainScreen();
		appOptionsScreen = mainScreen.selectAppOption();
		alertDialogsScreen = appOptionsScreen.selectAlertDialogs();
		alertDialogsScreen = alertDialogsScreen.openTextEntryDialog();
		alertDialogsScreen.typeUsername(username);
	}

	@Override
	public WebApp getTestedApp() {
		return apiDemosApp;
	}

}
