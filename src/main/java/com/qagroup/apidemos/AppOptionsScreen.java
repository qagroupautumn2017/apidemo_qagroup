package com.qagroup.apidemos;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;

public class AppOptionsScreen {

	@AndroidFindBy(accessibility = "Alert Dialogs")
	private MobileElement alertDialogsOption;

	private AndroidDriver<MobileElement> driver;

	public AppOptionsScreen(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@Step("Select Alert Dialogs")
	public AlertDialogsScreen selectAlertDialogs() {
		alertDialogsOption.click();
		return new AlertDialogsScreen(driver);
	}

}
