package com.qagroup.apidemos;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;

public class MainScreen {

	@AndroidFindBy(accessibility = "App")
	private MobileElement appOption;

	private AndroidDriver<MobileElement> driver;

	public MainScreen(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@Step("Select 'App'")
	public AppOptionsScreen selectAppOption() {
		appOption.click();
		return new AppOptionsScreen(driver);
	}
}
