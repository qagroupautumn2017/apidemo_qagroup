package com.qagroup.apidemos;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.qagroup.tools.WebApp;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class ApiDemosApp implements WebApp {

	private WebDriver driver;

	public ApiDemosApp() {
	}

	@Step("Open 'ApiDemos' on the Main screen")
	public MainScreen openMainScreen() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "android");
		capabilities.setCapability("platformVersion", "5.0.2");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 6P API 21");
		capabilities.setCapability(AndroidMobileCapabilityType.AVD, "Nexus_6P_API_21");
		capabilities.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + "\\ApiDemos-debug.apk");

		URL remoteAddress = null;
		try {
			remoteAddress = new URL("http://127.0.0.1:4723/wd/hub");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AndroidDriver<MobileElement> driver = new AndroidDriver<>(remoteAddress, capabilities);
		this.driver = driver;
		if(driver.isKeyboardShown()) {
			driver.hideKeyboard();
		}
		return new MainScreen(driver);
	}

	@Attachment("{screenshotName}")
	@Override
	public byte[] takeScreenshot(String screenshotName) {
		return ((TakesScreenshot) this.driver).getScreenshotAs(OutputType.BYTES);
	}

}
