package com.qagroup.apidemos;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;

public class AlertDialogsScreen {

	@AndroidFindBy(accessibility = "Text Entry dialog")
	private MobileElement textEntryDialog;

	@AndroidFindBy(id = "io.appium.android.apis:id/username_edit")
	private MobileElement usernameInput;

	private AndroidDriver<MobileElement> driver;

	public AlertDialogsScreen(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@Step("Open Text Entry Dialog")
	public AlertDialogsScreen openTextEntryDialog() {
		textEntryDialog.click();
		return this;
	}

	@Step("Enter username <{username}>")
	public void typeUsername(String username) {
		usernameInput.sendKeys(username);
	}

}
